#include "FractalTimestamps.h"
#include "arcdps.h"

#include <memory>
#include "Fractals.h"
#include <filesystem>
#include "string_format.hpp"
#include <vector>
#include <map>
#include <algorithm> // sort


using time_point = std::chrono::system_clock::time_point;
using system_clock = std::chrono::system_clock;
using std::pair;
using std::vector;
using std::map;
using std::string;

std::unique_ptr<FractalTimestamps> FractalTimestamps::fractalTimestamps;

bool FractalTimestamps::s_is_init() {
	return fractalTimestamps != NULL;
}


intptr_t FractalTimestamps::s_wnd_proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (!FractalTimestamps::s_is_init()) {
		return uMsg;
	}
	return FractalTimestamps::fractalTimestamps->WindowProc(hWnd, uMsg, wParam, lParam);
}

uintptr_t FractalTimestamps::s_combat(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	if (!FractalTimestamps::s_is_init()) {
		return 0;
	}
	return FractalTimestamps::fractalTimestamps->CombatEvent(ev, src, dst, skillname, id, revision);
}

uintptr_t FractalTimestamps::s_combat_local(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	if (!FractalTimestamps::s_is_init()) {
		return 0;
	}
	return FractalTimestamps::fractalTimestamps->CombatEventLocal(ev, src, dst, skillname, id, revision);
	return 0;
}

uintptr_t FractalTimestamps::s_imgui(uint32_t not_charsel_or_loading) {
	FractalTimestamps::fractalTimestamps->ImGui(not_charsel_or_loading);
	return 0;
}

void FractalTimestamps::s_uiOptions() {
	FractalTimestamps::fractalTimestamps->ui.options();
}


arcdps_exports* FractalTimestamps::s_init() {
	return FractalTimestamps::fractalTimestamps->Init();
}

void* FractalTimestamps::s_release() {
	if (fractalTimestamps->start_time != NULL) {
		fractalTimestamps->out_file.close();
		fractalTimestamps->start_time = NULL;
	}
	return 0;
}

uintptr_t FractalTimestamps::CombatEvent(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	if (!ev && dst && dst->self) {
		int mapid = mumbleApi.getMapId();
		if (fractalMaps.count(mapid) > 0 && fractalTimeIndices.count(mapid) == 0) {
			int index = fractaltimes.size();
			time_point now = system_clock::now();
			fractaltimes.push_back(std::pair<int, time_point>(mapid, now));
			fractalTimeIndices[mapid] = index;
			if (start_time != NULL) out_file << format_time_stamp(now) << " " << fractalMaps.at(mapid).name << std::endl;
			if (start_time != NULL) out_file.flush();
		}
	}
	return 0;
}



void FractalTimestamps::s_unofficial_extras(const UserInfo * pUpdatedUsers, uint64_t pUpdatedUsersCount) {
	if (!FractalTimestamps::s_is_init()) {
		return;
	}
	return FractalTimestamps::fractalTimestamps->UnofficialExtras(pUpdatedUsers, pUpdatedUsersCount);
}

map<string, MyExtraUserInfo> unofficial_extras_users;
vector<pair<string, std::chrono::milliseconds>> wait_per_player;
int ready_players = 0;
time_point wait_start;
bool waiting = false;
string waiting_for = "";

void FractalTimestamps::UnofficialExtras(const UserInfo* pUpdatedUsers, uint64_t pUpdatedUsersCount) {
	for (int i = 0; i < pUpdatedUsersCount; i++) {
		//UserInfo ui = ;
		string accNameStr = string(pUpdatedUsers[i].AccountName);
		// player remove
		if (pUpdatedUsers[i].Role == UserRole::None) {
			if (unofficial_extras_users.at(accNameStr).ReadyStatus) ready_players--;
			unofficial_extras_users.erase(pUpdatedUsers[i].AccountName);
			if (waiting_for == pUpdatedUsers[i].AccountName) {
				waiting_for = "";
				waiting = false;
			}
			continue;
		}
		// add new user
		if (unofficial_extras_users.count(accNameStr) != 1) {
			MyExtraUserInfo mui(accNameStr, pUpdatedUsers[i].Role, pUpdatedUsers[i].ReadyStatus);
			unofficial_extras_users.emplace(accNameStr,mui);
		}
		// update count
		if (pUpdatedUsers[i].ReadyStatus && !unofficial_extras_users.at(accNameStr).ReadyStatus) ready_players++; // player ready
		if (!pUpdatedUsers[i].ReadyStatus && unofficial_extras_users.at(accNameStr).ReadyStatus) ready_players--; // player unready
		// update rdy
		unofficial_extras_users.at(accNameStr).ReadyStatus = pUpdatedUsers[i].ReadyStatus;
		// waiting canceled
		if (waiting && ready_players < unofficial_extras_users.size() - 1) {  
			waiting = false;
			waiting_for = ""; // not required 
		}
		// waiting done
		if (waiting && ready_players == unofficial_extras_users.size()) { 
			waiting = false;
			bool found = false;
			std::chrono::milliseconds waiting_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - wait_start);
			for (vector<pair<string, std::chrono::milliseconds>>::iterator it = wait_per_player.begin(); it != wait_per_player.end(); it++) {
				if (waiting_for == it->first) {
					found = true;
					it->second += waiting_time;
					break;
				}
			}
			if (!found) {
				wait_per_player.push_back(std::pair<string, std::chrono::milliseconds>(waiting_for, waiting_time));
			}
			std::sort(wait_per_player.begin(), wait_per_player.end(), 
				[](pair<string, std::chrono::milliseconds> a, pair<string, std::chrono::milliseconds> b) {
					return a.second > b.second; 
				}
			);
			waiting_for = ""; // not required 
		}

	}
	// waiting start
	if (!waiting && ready_players == unofficial_extras_users.size() - 1) {
		waiting = true;
		wait_start = std::chrono::system_clock::now();
		for (auto& element : unofficial_extras_users) {
			if(!element.second.ReadyStatus)
				waiting_for = element.second.AccountName;
		}
	}
}

/** ??? */
uintptr_t FractalTimestamps::CombatEventLocal(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	return 0;
}

/* window callback -- return is assigned to umsg (return zero to not be processed by arcdps or game) */
uintptr_t FractalTimestamps::WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	return uMsg;
}

#define TIME_BUFF_LEN 64

string FractalTimestamps::format_time_stamp(time_point tp) {
	//char buff[256], time_buff[TIME_BUFF_LEN];
	std::time_t time_time = system_clock::to_time_t(tp);
	std::time_t start_time_time = system_clock::to_time_t(*start_time);
	struct tm time_tm, start_tm;
	localtime_s(&time_tm, &time_time);
	localtime_s(&start_tm, &start_time_time);
	int dsec = time_tm.tm_sec - start_tm.tm_sec;
	int dmin = time_tm.tm_min - start_tm.tm_min;
	if (dsec < 0) {
		dsec += 60;
		dmin -= 1;
	}
	int dhour = time_tm.tm_hour - start_tm.tm_hour;
	if (dmin < 0) {
		dmin += 60;
		dhour -= 1;
	}
	if (dhour < 0) {
		dhour = 0;
		dmin = 0;
		dsec = 0;
	}
	if (dhour > 0) {
		return string_format("%2d:%02d:%02d", dhour, dmin, dsec);
	} else {
		return string_format("%2d:%02d", dmin, dsec);
	}
}

/* ingui draw call */
uintptr_t FractalTimestamps::ImGui(uint32_t not_charsel_or_loading) {
	int map = mumbleApi.getMapId();
	ui.draw(fractalTimeIndices, fractaltimes, start_time, not_charsel_or_loading, map, wait_per_player, /*map<string, MyExtraUserInfo>()*/unofficial_extras_users, ready_players, waiting_for);
	// every 10s
	check_for_file_count++;
	if (check_for_file_count % 600 == 0) {
		if (checkForFile()) {
			for (std::pair<int, time_point> element : fractaltimes) {
				out_file << format_time_stamp(element.second) << " " << fractalMaps.at(element.first).name << std::endl;
				out_file.flush();
			}
		}
	}
	return 0;
}

arcdps_exports* FractalTimestamps::Init() {
	mumbleApi.initialize();
	arc_exports.sig = 0x69425;
	arc_exports.imguivers = 18000;
	arc_exports.size = sizeof(arcdps_exports);
	arc_exports.out_name = "FractalTimestamps";
	arc_exports.out_build = __DATE__;
	arc_exports.wnd_nofilter = FractalTimestamps::s_wnd_proc;
	arc_exports.wnd_filter = NULL;
	arc_exports.combat = FractalTimestamps::s_combat;
	arc_exports.combat_local = FractalTimestamps::s_combat_local;
	arc_exports.imgui = FractalTimestamps::s_imgui;
	arc_exports.options_windows = NULL;
	arc_exports.options_end = FractalTimestamps::s_uiOptions;
	start_time = NULL;
	load_time = system_clock::now();
	WriteArcLogFile("loaded FractalTimestamps\n");
	if (!fractalTimestamps) {
		WriteArcLogFile("[MAIN] init did not work :/");
	} else if (fractalTimestamps.get() != this) {
		WriteArcLogFile("[MAIN] init value is different");
	}
	return &arc_exports;
}

/*
 * FILETIME to std::chrono::system_clock::time_point (including milliseconds)
 */
time_point FileTime2TimePoint(const LPFILETIME ft) {
	SYSTEMTIME st = { 0 };
	if (!FileTimeToSystemTime(ft, &st)) {
		WriteArcLog("Invalid FILETIME");
		return time_point((time_point::min)());
	}

	// number of seconds 
	ULARGE_INTEGER ull;
	ull.LowPart = ft->dwLowDateTime;
	ull.HighPart = ft->dwHighDateTime;

	time_t secs = ull.QuadPart / 10000000ULL - 11644473600ULL;
	std::chrono::milliseconds ms((ull.QuadPart / 10000ULL) % 1000);

	auto tp = std::chrono::system_clock::from_time_t(secs);
	tp += ms;
	return tp;
}

bool FractalTimestamps::checkForFile() {
	if (start_time != NULL) return false;
	try {
		std::filesystem::path search_path = std::filesystem::path("D:/") / "a_OBS_Capture";
		for (auto const& file : std::filesystem::directory_iterator(search_path)) {
			if (!file.is_directory()) {
				FILETIME created_ft;
				HANDLE fileHandle = CreateFileA(file.path().u8string().c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
				bool got_time = GetFileTime(fileHandle, &created_ft, NULL, NULL);
				CloseHandle(fileHandle);
				if (got_time) {
					time_point created_tp = FileTime2TimePoint(&created_ft);
					if (created_tp > load_time) {
						start_time = std::make_unique<time_point>(created_tp);
						out_file.open(file.path().u8string() + ".txt", std::ios::out);
						return true; 
					}
				}
			}
		}
	} catch (std::exception e){

	}
	return false;
}
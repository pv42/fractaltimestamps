// dllmain.cpp : Definiert den Einstiegspunkt für die DLL-Anwendung.
#include <Windows.h>
#include <stdint.h>
#include <stdio.h>
#include <memory>
#include "arcdps.h"
#include "imgui/imgui.h"
#include "FractalTimestamps.h"
#include "unofficial_extras_definitions.h"

#define DLL_EXPORT extern "C" __declspec(dllexport)

/* dll attach -- from winapi */
void dll_init(HANDLE hModule) {
	return;
}

/* dll detach -- from winapi */
void dll_exit() {
	return;
}

/* dll main from winapi */
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
		dll_init(hModule);
		break;
	case DLL_PROCESS_DETACH:
		dll_exit();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return TRUE;
}

DLL_EXPORT void* get_init_addr(char* arcversionstr, ImGuiContext* imguicontext, void* id3dd9, HANDLE arcdll, void* mallocfn, void* freefn) {
	WriteArcLogFile("Loading FractalTimestamps ...\n");
	ImGui::SetCurrentContext((ImGuiContext*)imguicontext);
	ImGui::SetAllocatorFunctions((void* (*)(size_t, void*))mallocfn, (void (*)(void*, void*))freefn); // on imgui 1.80+
	FractalTimestamps::fractalTimestamps = std::make_unique<FractalTimestamps>(/*arcversionstr*/);
	return FractalTimestamps::s_init;
}

DLL_EXPORT void* get_release_addr() {
	WriteArcLogFile("unloading FractalTimestamps ...\n");
	return FractalTimestamps::s_release;
}

void extrasCallback(const UserInfo* pUpdatedUsers, uint64_t pUpdatedUsersCount) {
	FractalTimestamps::s_unofficial_extras(pUpdatedUsers, pUpdatedUsersCount);
}

DLL_EXPORT ExtrasSubscriberInitSignature* arcdps_unofficial_extras_subscriber_init(const ExtrasAddonInfo* pExtrasInfo, ExtrasSubscriberInfo* pSubscriberInfo) {
	if (pExtrasInfo && pExtrasInfo->ApiVersion == 1) {
		pSubscriberInfo->SubscriberName = "FractalTimestamps";
		pSubscriberInfo->SquadUpdateCallback = &extrasCallback;
	}
	return NULL;
}



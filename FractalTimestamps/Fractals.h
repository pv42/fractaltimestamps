#pragma once
#include <map>
#include <string>
#include <stdint.h>

struct FractalInfo {
	std::string name;
	uint32_t sigil1;
	uint32_t sigil2;
	uint32_t utility;
	FractalInfo(const char* n, uint32_t s1, uint32_t s2, uint32_t util) {
		name = n;
		sigil1 = s1;
		sigil2 = s2;
		utility = util;
	}
};

#define SIGIL_IMPACT 24868
#define SIGIL_FORCE 24615
#define SIGIL_AIR 24554
#define SIGIL_NIGHT 36053

// without superior sigil
const static std::map<int, std::string> sigils = {
	{SIGIL_NIGHT, "Night"},
	{24678, "Justice"}, // Bandit Slaying 
	{24667, "Wrath"},   // Sons of Svanier Slaying
	{24648, "Grawl Slaying"},
	{24684, "Sorrow"}, // Dredge Slaying
	{24661, "Elemental Slaying"},
	{24658, "Serpend Slaying"},
	{SIGIL_AIR, "Air"},
	{SIGIL_IMPACT, "Impact"},
	{SIGIL_FORCE, "Force"},
};

// these names are shortend
const static std::map<int, std::string> utilities = {
	{9443, "Sharpening Stones"},
	{50082, "Scarlet's Armies Slaying"},
	{8881, "Outlaw Slaying"}, 
	{8883, "Sons of Svanier Slaying"},
	{8890, "Grawl Slaying"},
	{8887, "Inquest Slaying"},
	{8892, "Dredge Slaying"},
	{8885, "Elemental Slaying"},
};

const static std::map<int, FractalInfo> fractalMaps = {
	{951, FractalInfo("Aquatic Ruins", SIGIL_FORCE, 24658, 50082)},
	{1309, FractalInfo("Sirens Reef", SIGIL_FORCE, SIGIL_IMPACT, 9443)},
	{958, FractalInfo("Solid Ocean", SIGIL_NIGHT, 24661, 8885)},
	{953, FractalInfo("Underground Facility",SIGIL_NIGHT, 24684, 8892)},
	{957, FractalInfo("Thaumanova Reactor", SIGIL_FORCE, SIGIL_IMPACT, 9443)},
	{955, FractalInfo("Molten Furnace", SIGIL_IMPACT, SIGIL_AIR, 50082)},
	{1290, FractalInfo("Deepstone", SIGIL_FORCE, SIGIL_AIR, 9443)},
	{950, FractalInfo("Urban Battleground", SIGIL_FORCE, SIGIL_IMPACT, 9443)},
	{1267, FractalInfo("Twilight Oasis", SIGIL_NIGHT, SIGIL_IMPACT, 9443)},
	{949, FractalInfo("Swampland", SIGIL_NIGHT, SIGIL_IMPACT, 9443)},
	{959, FractalInfo("Molten Boss", SIGIL_FORCE, SIGIL_IMPACT, 50082)},
	{947, FractalInfo("Uncategorized", SIGIL_FORCE, SIGIL_IMPACT, 8887)},
	{954, FractalInfo("Volcanic", SIGIL_NIGHT, 24648, 8890)},
	{948, FractalInfo("Snowblind", SIGIL_NIGHT, 24667, 8883)},
	{952, FractalInfo("Cliffside", SIGIL_FORCE, 24678, 8881)},
	{960, FractalInfo("Captain Mai Trin Boss", SIGIL_FORCE, SIGIL_IMPACT, 50082)},
	{956, FractalInfo("Aetherblade", SIGIL_FORCE, SIGIL_IMPACT, 50082)},
	{1164, FractalInfo("Chaos", SIGIL_FORCE, SIGIL_IMPACT, 9443)},
	{1177, FractalInfo("Nightmare", SIGIL_FORCE, SIGIL_IMPACT, 50082)},
	{1205, FractalInfo("Shattered Observatory", SIGIL_FORCE, SIGIL_IMPACT, 9443)},
	{1384, FractalInfo("Sunqua Peak", SIGIL_NIGHT, SIGIL_IMPACT, 9443)}
};
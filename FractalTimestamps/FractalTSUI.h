#pragma once
#include <stdint.h>
#include <chrono>
#include <map>
#include <vector>
#include <memory>
#include <string>
#include "unofficial_extras_definitions.h"

using time_point = std::chrono::system_clock::time_point;

class FractalTSUI {
public:
	FractalTSUI();
	void draw(std::map<int, int> fractalTimeIndices, std::vector<std::pair<int, time_point>> fractaltimes, std::shared_ptr<time_point> start_time, uint32_t not_charsel_or_loading,
		int currentMap, std::vector <std::pair<std::string, std::chrono::milliseconds>> wait_per_player, std::map<std::string, MyExtraUserInfo> myextrainfo, int ready_players, std::string waiting_for);
	void options();
private:
	bool ts_visible;
	bool cs_visible;
	bool rc_wait_visible;
};


#include "FractalTSUI.h"
#include "imgui/imgui.h"
#include <stdint.h>
#include "Fractals.h"
#include <ctime>
#include <vector>
#include <memory>
#include "string_format.hpp"
#include "FractalTimestamps.h"

using time_point = std::chrono::system_clock::time_point;
using system_clock = std::chrono::system_clock;

FractalTSUI::FractalTSUI() : cs_visible(true), rc_wait_visible(true) {

}


void FractalTSUI::draw(std::map<int, int> fractalTimeIndices, std::vector<std::pair<int, time_point>> fractaltimes, std::shared_ptr<time_point> start_time, uint32_t not_charsel_or_loading, int currentMap, std::vector<std::pair<std::string, std::chrono::milliseconds>> wait_per_player, std::map<std::string, MyExtraUserInfo> myextrausers, int ready_players, std::string waiting_for) {
	if (not_charsel_or_loading) {
		if(ts_visible) {
			if(ImGui::Begin("Fractal Timestamps", &ts_visible)) {
			if (start_time != NULL) {
				for (std::pair<int, time_point> element : fractaltimes) {
					char buff[256];
					std::time_t time_time = std::chrono::system_clock::to_time_t(element.second);
					std::time_t start_time_time = system_clock::to_time_t(*start_time);
					struct tm time_tm, start_tm;
					localtime_s(&time_tm, &time_time);
					localtime_s(&start_tm, &start_time_time);
					int dsec = time_tm.tm_sec - start_tm.tm_sec;
					int dmin = time_tm.tm_min - start_tm.tm_min;
					if (dsec < 0) {
						dsec += 60;
						dmin -= 1;
					}
					int dhour = time_tm.tm_hour - start_tm.tm_hour;
					if (dmin < 0) {
						dmin += 60;
						dhour -= 1;
					}
					if (dhour < 0) {
						dhour = 0;
						dmin = 0;
						dsec = 0;
					}
					if (dhour > 0) {
						sprintf_s(buff, "%02d:%02d:%02d %s", dhour, dmin, dsec, fractalMaps.at(element.first).name.c_str());
					} else {
						sprintf_s(buff, "   %02d:%02d %s", dmin, dsec, fractalMaps.at(element.first).name.c_str());
					}
					ImGui::Text(buff);
				}
			} else {
				ImGui::Text("No start time yet :/");
			}
		}
			ImGui::End();
		}
		if (cs_visible && fractalMaps.count(currentMap) > 0) {
			if (ImGui::Begin("Cheat Sheet", &cs_visible, ImGuiWindowFlags_NoTitleBar)) {
				FractalInfo fractal = fractalMaps.at(currentMap);
				ImGui::Text(fractal.name.c_str());
				std::string s1str = sigils.count(fractal.sigil1) > 0 ? sigils.at(fractal.sigil1) : string_format("?(%d)", fractal.sigil1);
				std::string s2str = sigils.count(fractal.sigil2) > 0 ? sigils.at(fractal.sigil2) : string_format("?(%d)", fractal.sigil2);
				ImGui::Text(string_format("Sigils: %s/%s", s1str.c_str(), s2str.c_str()).c_str());
				std::string utilstr = utilities.count(fractal.utility) > 0 ? utilities.at(fractal.utility) : string_format("?(%d)", fractal.utility);
				ImGui::Text(string_format("Utility: %s", utilstr.c_str()).c_str());
			}
			ImGui::End();
		}
		if (rc_wait_visible) {
			if (ImGui::Begin("RC last wait", &rc_wait_visible)) {
				std::vector<std::pair<std::string, std::chrono::milliseconds>>::iterator it;
				if (wait_per_player.size() == 0) {
					ImGui::Text("no wait yet");
				}
				for (it = wait_per_player.begin(); it != wait_per_player.end(); it++) {
					int mins = (it->second).count() / 60000;
					int secs = ((it->second).count() / 1000) % 60;
					int ms = (it->second).count() % 1000;
					ImGui::Text("%3d:%02d.%03d %s", mins, secs, ms, (it->first).c_str());
				}
				ImGui::Separator();
				ImGui::Text("%d/%d ready, waiting for '%s'", ready_players, myextrausers.size(), waiting_for.c_str());
				/*ImGui::Separator();
				std::map<std::string, MyExtraUserInfo>::iterator et;
				for (et = myextrausers.begin(); et != myextrausers.end(); et++) {
					if ((et->second).ReadyStatus) {
						ImGui::Text("RDY  ");
					} else {
						ImGui::Text("NTR  ");
					}
					ImGui::SameLine();
					//ImGui::Text(" ");
					//ImGui::SameLine();
					//ImGui::Text((et->second).AccountName.c_str());
					//ImGui::SameLine();
					//ImGui::Text(" ");
					//ImGui::SameLine();
					ImGui::Text((et->first).c_str());
				}*/
			}
			ImGui::End();
		}
	}
}

void FractalTSUI::options() {
	ImGui::Checkbox("Fractal Timstamps", &ts_visible);
	ImGui::Checkbox("Cheat Sheet", &cs_visible);
	ImGui::Checkbox("Ready Check wait time", &rc_wait_visible);
}

#pragma once
#include <wtypes.h>
#include <memory>
#include "arcdps.h"
#include "MumbleApi.h"
#include "arcdps.h"
#include "FractalTSUI.h"
#include <vector>
#include <fstream>
#include "unofficial_extras_definitions.h"

using time_point = std::chrono::system_clock::time_point;

class FractalTimestamps {
public:
	static std::unique_ptr<FractalTimestamps> fractalTimestamps;
	static arcdps_exports* s_init(); 
	static void* s_release();
	static uintptr_t s_imgui(uint32_t not_charsel_or_loading);
	static void s_uiOptions();
	static void s_unofficial_extras(const UserInfo* pUpdatedUsers, uint64_t pUpdatedUsersCount);
	arcdps_exports* Init();
	uintptr_t Release();
protected:
	uintptr_t CombatEvent(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);
	/** ??? */
	uintptr_t CombatEventLocal(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);
	/* window callback -- return is assigned to umsg (return zero to not be processed by arcdps or game) */
	uintptr_t WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	/* ingui draw call */
	uintptr_t ImGui(uint32_t not_charsel_or_loading);
	void UnofficialExtras(const UserInfo* pUpdatedUsers, uint64_t pUpdatedUsersCount);
private:
	static bool s_is_init();
	static intptr_t s_wnd_proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	static uintptr_t s_combat(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);
	static uintptr_t s_combat_local(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);

	bool checkForFile();
	std::string format_time_stamp(time_point tp);

	int check_for_file_count;
	arcdps_exports arc_exports;
	MumbleApi mumbleApi;
	FractalTSUI ui;
	std::map<int, int> fractalTimeIndices;
	std::vector<std::pair<int, time_point>> fractaltimes;
	time_point load_time;
	std::shared_ptr<time_point> start_time;
	std::fstream out_file;
};



